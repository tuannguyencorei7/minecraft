export class Supportonline {
    id?: Number;
    siteid?: string;
    typeid?: string;
    nick?: string;
    name?: string;
    approved?: string;
    arrange?: string;
    note?: string;
    phone?: string;
}
