export class News {
    id?: Number;
    typeid?: string;
    siteid?: string;
    title?: string;
    url?: string;
    postdate?: string;
    changedate?: string;
    summary?: string;
    content?: string;
    pathimage?: string;
    author?: string;
    approved?: string;
    comment?: string;
    lang?: string;
    isfocus?: string;
    isNew?: string;
    view?: string;
}
