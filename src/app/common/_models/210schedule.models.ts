export class Schedule {
    id?: Number;
    sdate?: string;
    stime?: string;
    content?: string;
    stakeholder?: string;
    location?: string;
    host?: string;
    approved?: string;
    postdate?: string;
    lang?: string;
}
