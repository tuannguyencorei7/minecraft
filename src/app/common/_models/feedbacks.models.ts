export class Feedbacks {
    id?: Number;
    siteid?: string;
    name?: string;
    email?: string;
    phone?: string;
    address?: string;
    title?: string;
    content?: string;
    approved?: string;
    lang?: string;
    postdate?: string;
    reply?: string;
}
