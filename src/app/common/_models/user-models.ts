export class User {
    id?: Number;
    company?: Number;
    name?: string;
    nameTemp?: string;
    number_seat?: string;
    phone?: string;
    cmnd?: string;
    address?: string;
    born?: string;
}