export class Photos {
    id?: Number;
    typeid?: string;
    title?: string;
    pathimage?: string;
    postdate?: string;
    approved?: string;
    lang?: string;
    arrange?: string;
}
