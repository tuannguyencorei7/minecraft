export class Sys_banner {
    id?: Number;
    siteid?: string;
    title?: string;
    link?: string;
    arrange?: string;
    approved?: string;
    lang?: string;
    pathimage?: string;
}
