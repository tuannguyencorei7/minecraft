export class Documents {
    id?: Number;
    typeid?: string;
    siteid?: string;
    title?: string;
    url?: string;
    content?: string;
    pathfile?: string;
    postdate?: string;
    changedate?: string;
    approved?: string;
    lang?: string;
    author?: string;
}
