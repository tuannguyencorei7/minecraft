export class Advertises {
    id?: Number;
    siteid?: string;
    title?: string;
    pathimage?: string;
    position?: string;
    approved?: string;
    arrange?: string;
    link?: string;
    lang?: string;
}
