export class Sys_configsite {
    id?: Number;
    siteid?: string;
    title?: string;
    keywords?: string;
    description?: string;
    footer?: string;
    approved?: string;
    lang?: string;
}
