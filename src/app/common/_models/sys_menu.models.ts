export class Sys_menu {
    id?: Number;
    ptypeid?: string;
    siteid?: string;
    title?: string;
    keyword?: string;
    arrange?: string;
    position?: string;
    approved?: string;
    lang?: string;
    url?: string;
    intranet?: string;
}
