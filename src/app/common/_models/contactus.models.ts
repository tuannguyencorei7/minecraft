export class Contactus {
    id?: Number;
    siteid?: string;
    title?: string;
    content?: string;
    approved?: string;
    lang?: string;
    email?: string;
}
