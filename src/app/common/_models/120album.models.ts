export class Album {
    id?: Number;
    siteid?: string;
    title?: string;
    arrange?: string;
    approved?: string;
    lang?: string;
    pathimage?: string;
    description?: string;
    postdate?: string;
}
