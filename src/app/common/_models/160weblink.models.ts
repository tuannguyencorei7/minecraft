export class Weblink {
    id?: Number;
    siteid?: string;
    title?: string;
    link?: string;
    approved?: string;
    arrange?: string;
    lang?: string;
}
