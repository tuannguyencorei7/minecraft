export class Contents {
    id?: Number;
    typeid?: string;
    siteid?: string;
    title?: string;
    url?: string;
    summary?: string;
    content?: string;
    pathfile?: string;
    pathimage?: string;
    comment?: string;
    postdate?: string;
    changedate?: string;
    approved?: string;
    lang?: string;
    author?: string;
}
